using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Test_Driven_Development;

namespace BusinessTests
{
    [TestClass]
    public class BusinessTests
    {
        [TestMethod]
        public void EmployeeSuccessfullyAddedToList()
        {
            // Arrange
            Business business = new Business();

            // Act
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));

            // Assert
            Assert.AreEqual(2, business.Employees.Count);
        }

        [TestMethod]
        public void JobAddedToListSuccessfullyTest()
        {
            // Arrange
            Business business = new Business();

            // Act
            business.AddJob(new Job(5));
            business.AddJob(new Job(10));

            // Assert
            Assert.AreEqual(2, business.Jobs.Count);
        }

        [TestMethod]
        public void JobCompleteAfterWorkTest()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(2));
            business.AddEmployee(new Employee(10, 5));

            // Act
            business.DoWork();

            // Assert
            Assert.IsTrue(business.Jobs[0].JobCompleted);
        }

        [TestMethod]
        public void JobIncompleteWhenOutOfEmployees()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(15));
            business.AddEmployee(new Employee(10, 5));
            business.AddEmployee(new Employee(10, 7));

            // Act
            business.DoWork();

            // Assert
            Assert.IsFalse(business.Jobs[0].JobCompleted);
        }
        [TestMethod]
        public void SecondJobIncComplete()
        {
            // Arrange 
            Business business = new Business();
            Job job = new Job(9);
            Job job2 = new Job(8);
            Employee employee = new Employee(10, 13);
            business.AddJob(job);
            business.AddJob(job2);
            business.AddEmployee(employee);

            // Act
            business.DoWork();

            // Assert
            Assert.IsTrue(business.Jobs[0].JobCompleted && business.Jobs[1].JobCompleted == false);
        }

        [TestMethod]
        public void EmployeePayment()
        {
            // Arrange
            Business business = new Business();
            business.AddJob(new Job(10));
            Employee employee = new Employee(10, 12);
            business.AddEmployee(employee);
            decimal payment = 100;

            // Act
            business.DoWork();
            decimal actPay = employee.Paycheck;

            // Assert
            Assert.AreEqual(payment, actPay);
        }

        [TestMethod]
        public void JobCost()
        {
            // Arrange
            Business business = new Business();
            Job job = new Job(10);
            business.AddJob(job);
            Employee employee = new Employee(10, 12);
            business.AddEmployee(employee);
            decimal JobCost = 150;

            // Act
            business.DoWork();
            decimal actJobCost = job.JobCost;

            // Assert
            Assert.AreEqual(JobCost, actJobCost);
        }
    }
}
