﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    [Serializable]
    public class Employee
    {
        private decimal hourlyWage;

        private int hoursScheduled;

        private Employee() 
        {
        }

        public Employee(decimal hourlyWage, int hoursScheduled)
        {
            this.hourlyWage = hourlyWage;
            this.hoursScheduled = hoursScheduled;
        }

        public decimal Paycheck { get; set; }

        public void DoWork(Job work)
        {
            // If the hoursScheduled are less than the time remaining on the job, reduce the hours remaining on the job by the hours scheduled, and set hours scheduled to zero.
            // Else reduce hours scheduled by time remaining, and set time investment for job to 0.
            if (hoursScheduled < work.TimeInvestmentRemaining)
            {
                work.TimeInvestmentRemaining -= hoursScheduled;
                this.Paycheck = hoursScheduled * this.hourlyWage;
                hoursScheduled = 0;
            }
            else 
            {
                hoursScheduled -= work.TimeInvestmentRemaining;
                this.Paycheck = work.TimeInvestmentRemaining * this.hourlyWage;
                work.TimeInvestmentRemaining = 0;
            }
        }
    }
}
