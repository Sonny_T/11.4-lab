﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    [Serializable]
    public class Business
    {
        public Business()
        {
            this.Employees = new List<Employee>();
            this.Jobs = new List<Job>();
        }

        //public Business() 
        //{
        //}

        public List<Employee> Employees { get; set; }

        public List<Job> Jobs { get; set; }

        public void AddEmployee(Employee employee)
        {
            // Add employee to the employee list.
            this.Employees.Add(employee);
        }

        public void AddJob(Job job)
        {
            // Add job to the job list
            this.Jobs.Add(job);
        }

        public void DoWork()
        {
            // For each job, if job isn't completed, check each employees to see if they have hours and have them DoWork(). If job is completed after work, break from loop.
            foreach (Job j in this.Jobs) 
            {
                if (j.JobCompleted == false) 
                {
                    foreach (Employee e in this.Employees) 
                    {
                        e.DoWork(j);
                        j.JobCost = e.Paycheck * (decimal)1.5;
                        if (j.JobCompleted == true) 
                        {
                            break;
                        }
                    }
                }
            }
        }
    }
}
