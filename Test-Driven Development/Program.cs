﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace Test_Driven_Development
{
    class Program
    {
        static void Main(string[] args)
        {
            Business business = new Business();
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));
            business.AddJob(new Job(15));
            business.DoWork();

            SerializeBusiness(business);
            Console.ReadLine();
        }

        static void SerializeBusiness(Business business) 
        {
            // Create a new XmlSerializer instance with the type of the test class
            XmlSerializer seralizer = new XmlSerializer(typeof(Business));

            // Create a file using the passed-in file name
            // Use a using statement to automatically clean up object references
            // and close the file handle when the serialization process is complete
            using (Stream stream = File.Create(@"C:\ADN\bussiness.bin"))
            {
                // Serialize (save) the current instance of the zoo
                seralizer.Serialize(stream, business);
            }
        }
    }
}
