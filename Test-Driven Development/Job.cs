﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    [Serializable]
    public class Job
    {
        public Job(int timeInvestment)
        {
            this.TimeInvestmentRemaining = timeInvestment;
        }

        public decimal JobCost { get; set; }

        public int TimeInvestmentRemaining { get; set; }

        public bool JobCompleted => this.TimeInvestmentRemaining == 0;

        private Job()
        {
        }
    }
}
